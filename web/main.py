import time
import sys
from fastapi import FastAPI
from starlette.responses import StreamingResponse
import asyncio

app = FastAPI()

INTERVAL = 10

@app.get("/")
async def root():
    return {"message": "Hello World"}

@app.get("/sleep")
async def sleep(seconds: int = 0):
    print(f"Received request: Sleeping for {seconds} seconds")
    sys.stdout.flush()

    rounds = 0
    remaining = seconds
    while remaining > 0:
        print(f"Sleeping: {rounds*INTERVAL} of {seconds} seconds")
        sys.stdout.flush()

        time.sleep(min(INTERVAL,remaining))

        remaining -= INTERVAL
        rounds += 1

    print(f"Slept for {seconds} seconds")
    sys.stdout.flush()
    return {"message": f"Slept for {seconds} seconds"}

async def stream_data(seconds: int):
    rounds = 0
    remaining = seconds
    while remaining > 0:
        print(f"Streaming for {rounds*INTERVAL} of {seconds} seconds")
        sys.stdout.flush()

        yield f"data: Current time {time.time()}\n\n"
        await asyncio.sleep(min(INTERVAL,remaining))

        remaining -= INTERVAL
        rounds += 1

    print(f"Streamed for {seconds} seconds")
    sys.stdout.flush()
    yield f"Streamed for {seconds} seconds"

@app.get("/stream")
async def stream(seconds: int= 0):
    print(f"Received request: Stream for {seconds} seconds")
    sys.stdout.flush()
    return StreamingResponse(stream_data(seconds), media_type="text/plain")
